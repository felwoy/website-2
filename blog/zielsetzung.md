+++
title = "Zielsetzung"
date = "2018-03-10T10:00:00+02:00"
tags = ["Ziel","Planung"]
categories = ["Projektmanagement"]
+++

## Projektzielsetung

Bis zum 14. Mai 2018 soll im Rahmen der Software Engineering 1 Vorlesung eine Website erstellt werden. Die primäre Aufgabe dieser Website wird es sein, Erwähnungen ausgewählter Begriffe oder Namen in der online verfügbaren Presse regelmäßig in einem Dokument zusammenzustellen. Dies ist unter dem Namen "Pressespiegel" bekannt.
Die Auswahl an Begrifflichkeiten soll durch den Nutzer individuell festgelegt und modifiziert werden können. Notwendig hierfür ist eine adäquate Nutzerverwaltung. Gleichzeitig soll neben den gewünschten Themen ebenso die Auswahl der zu durchsuchenden Quellen angepasst werden können.
Das System soll als Service auf einer Website erstellt werden und mittles einer passenden Benutzeroberfläche bedient werden können.