+++
title = "Template"
date = "2018-03-08T22:22:22+02:00"
tags = ["Template"]
categories = ["Template"]
banner = "blog/somepicture.png"
+++

## Überschrift (Leerzeichen nicht vergessen)

Text
Text
[Verlinkung zu Datei](/blog/filePath)
### Unterüberschrift

*Text
..*Text

Some Code:

``` 
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```


